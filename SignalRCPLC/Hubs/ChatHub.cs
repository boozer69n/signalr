﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRCPLC.Hubs
{
    public class ChatHub : Hub
    {
        // This is in-memory .. persist as per needs.
        private static Dictionary<string, ChatClient> ConnectedClientsList = new Dictionary<string, ChatClient>();

        public List<ChatClient> JoinChatroom(string username, string room)
        {
            // Add client & broadcast new chat user message to all clients connected to this Hub.
            ChatClient newChatClient = new ChatClient()
            {
                ChatUserName = username,
                ClientId = Context.ConnectionId,
                ChatRoom = room,
            };
            ConnectedClientsList.Add(newChatClient.ClientId, newChatClient);
            //Clients.All.addChatMessage(newChatClient.ChatUserName + " has joined the Chatroom!");

            // Organizing clients in groups ..
            Groups.Add(newChatClient.ClientId, room);

            Clients.Group(room, newChatClient.ClientId).addUserList(username);

            PushServerMessageToClients(newChatClient.ChatUserName + " has joined the Chatroom!", newChatClient.ChatRoom);

            return GetRoomClientList(room);
        }

        public bool UserExist(string username)
        {
            int count = ConnectedClientsList.Values.Count(chatClient => chatClient.ChatUserName.Equals(username, StringComparison.InvariantCultureIgnoreCase));
            return count == 1;
        }

        public List<ChatClient> GetRoomClientList(string room)
        {
            return ConnectedClientsList.Values.Where(chatClient => chatClient.ChatRoom.Equals(room, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            LeaveChatRoom();
            return base.OnDisconnected(stopCalled);
        }

        public void LeaveChatRoom()
        {
            if (ConnectedClientsList.ContainsKey(Context.ConnectionId))
            {
                ChatClient client = ConnectedClientsList[Context.ConnectionId];
                // Clean-up.
                ConnectedClientsList.Remove(client.ClientId);
                PushServerMessageToClients(client.ChatUserName + " has left the Chatroom!", client.ChatRoom);

                Groups.Remove(client.ClientId, "Main Lobby");

                Clients.Group(client.ChatRoom).removeUserList(client.ChatUserName);
            }
        }
        
        public void PushChatMessageToClients(string message)
        {
            if (ConnectedClientsList.ContainsKey(Context.ConnectionId))
            {
                ChatClient client = ConnectedClientsList[Context.ConnectionId];
                Clients.Group(client.ChatRoom).addChatMessage(message, client.ChatUserName);
            }        
        }
        
        public void PushServerMessageToClients(string message, string room)
        {
            Clients.Group(room).addChatMessage(message, "Server");
        }
    }
}