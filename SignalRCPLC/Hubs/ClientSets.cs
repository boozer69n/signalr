﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRCPLC.Hubs
{
    public class ChatClient
    {
        public string ClientId { get; set; }
        public string ChatUserName { get; set; }
        public string ChatRoom { get; set; }
    }
}